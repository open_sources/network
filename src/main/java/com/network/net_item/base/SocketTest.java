package com.network.net_item.base;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 * <p>Title: SocketTest</p >
 * <p>Description: socket测试代码</p >
 * <p>Company: http://www.weiqi.com</p >
 * <p>Project: net_item</p >
 *
 * @author: weiqi
 * @Date: 2019-08-22 11:11
 * @Version: 1.0
 */
@Slf4j
public class SocketTest {

    public static void main(String[] args) {

        //obtainDateTime();

        //obtainDataTimeOut();

        socketConstructor();
    }

    /**
     * @description: 获取"当日时间"服务，"time-A.timefreq.bldrdoc.gov"服务器位于美国科罗拉多州的国家标准与技术研究所
     * @auther: weiqi
     * @date: 2019-08-22 11:29
     */
    public static void obtainDateTime() {
        try (Socket socket = new Socket("time-A.timefreq.bldrdoc.gov", 13)) {
            InputStream inputStream = socket.getInputStream();
            Scanner scanner = new Scanner(inputStream);

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                log.info("line is : " + line);
            }
        } catch (IOException e) {
            log.error("obtain data error : {} " , e);
        }
    }

    /**
     * @description: socket超时设置，在实际开发中，要根据不同系统的要求，设置超时时间
     * @auther: weiqi
     * @date: 2019-08-22 11:42
     */
    public static void obtainDataTimeOut() {
        try (Socket socket = new Socket("www.baidu.com", 80)) {
            socket.setSoTimeout(1000);
            //从套接字读取信息时，在确认是否有数据访问之前，读操作（read）会被阻塞；如果此时我们访问的主机不可达，那么应用将要等待很长时间，
            //因为底层操作系统做了限制，所以最终会超时，这里访问百度服务器就是一个例子
            InputStream inputStream = socket.getInputStream();
            Scanner scanner = new Scanner(inputStream);

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                log.info("line is : " + line);
            }
        } catch (InterruptedIOException iie) {
            log.error("obtain data error : {} " , iie);
        } catch (IOException ie) {
            log.error("obtain data error : {} " , ie);
        }
    }

    /**
     * @description: Socket(host, port)这个构造方法，如果连接不到主机，会一直阻塞下去，指导建立了到达主机的初始连接为止；在实际使用的时候，需要先建立一个无连接的套接字，
     * 然后使用套接字的方法connect()添加超时时间来解决这个问题。
     * @auther: weiqi
     * @date: 2019-08-22 11:51
     */
    public static void socketConstructor() {
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress("time-A.timefreq.bldrdoc.gov", 13), 3000);

            log.info("socket is not connection : " + socket.isConnected());

            log.info("socket is not closed : " + socket.isClosed());

            InputStream inputStream = socket.getInputStream();

            Scanner scanner = new Scanner(inputStream);

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                log.info("line data is : " + line);
            }

            socket.close();

            log.info("socket is not closed : " + socket.isClosed());
        } catch (IOException ie) {
            log.error("obtain data error : {} " , ie);
        }

    }
 }
