package com.network.net_item.base;

import lombok.extern.slf4j.Slf4j;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * <p>Title: InetAddress</p >
 * <p>Description: InetAddress test</p >
 * <p>Company: http://www.weiqi.com</p >
 * <p>Project: net_item</p >
 *
 * @author: weiqi
 * @Date: 2019-08-23 14:00
 * @Version: 1.0
 */
@Slf4j
public class InetAddressTest {

    public static void main(String[] args) {
        obtainInetAddress();
    }

    /**
     * @description: InetAddress方法
     * @auther: weiqi
     * @date: 2019-08-23 14:04
     */
    public static void obtainInetAddress() {

        try {

            //获取没有负载均衡的主机
            InetAddress inetAddress = InetAddress.getByName("time-A.timefreq.bldrdoc.gov");

            log.info("address is : " + inetAddress);

            byte[] addressBytes = inetAddress.getAddress();

            log.info(" : " + addressBytes.hashCode());

            //获取负载均衡的主机
            InetAddress[] inetAddresses = InetAddress.getAllByName("google.com");

            for (InetAddress inetAddress1 : inetAddresses) {
                log.info("inet address is : " + inetAddress1);
            }

            //获取本机ip地址
            InetAddress localhost = InetAddress.getLocalHost();

            log.info("local host is : " + localhost);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
