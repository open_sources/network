package com.network.net_item.socket;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * <p>Title: ServerEndPoint</p >
 * <p>Description: socket创建简单的服务器</p >
 * <p>Company: http://www.novabeyond.com</p >
 * <p>Project: net_item</p >
 *
 * @author: weiqi
 * @Date: 2019-08-23 14:25
 * @Version: 1.0
 */
@Slf4j
public class ServerEndPoint {

    public static void main(String[] args) {
        establishServer();
    }

    /**
     * @description: 创建服务器
     * @auther: weiqi
     * @date: 2019-08-23 14:31
     */
    public static void establishServer() {

        try (
                ////创建一个监听8088端口的套接字
                ServerSocket serverSocket =new ServerSocket(8088)
        ){

            //accept等待连接，该方法阻塞当前线程，直到建立连接，该方法返回一个socket套接字
            Socket socketIn = serverSocket.accept();

            InputStream inputStream = socketIn.getInputStream();
            OutputStream outputStream = socketIn.getOutputStream();

            Scanner scanner = new Scanner(inputStream);

            PrintWriter printWriter = new PrintWriter(outputStream, true);

            printWriter.println("Please enter bye to exit!");

            boolean done = false;

            while (!done && scanner.hasNextLine()) {
                String line = scanner.nextLine();
                printWriter.println("echo : " + line);
                if ("bye".equals(line.trim())) {
                    done = true;
                }
            }

        } catch (IOException ioe) {
            log.error("server error : " + ioe);
        }
    }
}
