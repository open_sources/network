package com.network.net_item.socket;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 * <p>Title: ClientEndPoint</p >
 * <p>Description: 客户端</p >
 * <p>Company: http://www.novabeyond.com</p >
 * <p>Project: net_item</p >
 *
 * @author: weiqi
 * @Date: 2019-08-23 14:38
 * @Version: 1.0
 */
@Slf4j
public class ClientEndPoint {

    public static void main(String[] args) {
        establishClient();
    }

    /**
     * @description: 创建客户端
     * @auther: weiqi
     * @date: 2019-08-23 14:52
     */
    public static void establishClient() {


        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress("localhost", 8088), 3000);

            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();

            Scanner scanner = new Scanner(System.in);

            PrintWriter printWriter = new PrintWriter(outputStream, true);

            printWriter.println(scanner.next());

            Scanner scanner1 = new Scanner(inputStream);

            while (scanner1.hasNextLine()) {
                String line = scanner.nextLine();
                log.info("line info is : " + line);
            }
        } catch (IOException ioe) {
            log.error("establish client error : " + ioe);
        }
    }
}
